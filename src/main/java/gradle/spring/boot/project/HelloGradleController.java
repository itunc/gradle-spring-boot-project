package gradle.spring.boot.project;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class HelloGradleController {

    @GetMapping
    public String helloGradle() {
        return "Hello Gradle!";
    }


    @RequestMapping("/incredible")
    public String helloIncredible() {
        return "Hello Incredible!";
    }

}